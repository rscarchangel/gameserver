#!/bin/bash
ant compile

rm MD5CHECKSUM
cp RSCArchangel_Client.jar Cache/
#md5sum RSCArchangel_Client.jar|awk {'print $2"="$1'} >> MD5CHECKSUM

cd Cache
FILES=`ls .`
for file in $FILES
do
	md5sum $file|awk {'print $2"="$1'} >> ../MD5CHECKSUM
done
